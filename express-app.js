/**
 * Module dependencies.
 */

var express = require('express'),
    app = module.exports = express(),
    fs = require('fs'),
    less = require('less-middleware'),
    path = require('path'),
    port = process.env.PORT || 2221;
    
app.configure(function(){
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.compress());
    app.engine('.html', require('ejs').__express);

    app.set('views',__dirname + '/');
    app.set('view engine', 'html');
    app.use(less(path.join(__dirname),{
        debug : true,
        force : true
    }));

    //static files
    app.use("/bower_components",express.static(__dirname + '/bower_components'));
    app.use("/templates",express.static(__dirname + '/templates'));    
    app.use("/resources",express.static(__dirname + '/resources'));
    //app.use("/js",express.static(__dirname + '/js'));
    app.use(app.router);
});

app.get('/', function(req, res) {
    res.render('index');
});

console.log("Express server listening on port " + port);

app.listen(port);
var ngSubwayFreshTracks = angular.module('ngSubwayFreshTracks', [
        'ui.router',
        'ui.bootstrap',
        'ngSanitize',
        'ngSubwayFreshTracks.config',
        'efsharp.ads.directive.loadingspinner',
        'efsharp.ads.directive.video',
        'efsharp.ads.directive.facebook',
        'efsharp.ads.directive.progresscontrol',
        'efsharp.ads.directive.twitter',
        'efsharp.ads.service.social',
        'efsharp.ads.service.cms',
        'efsharp.ads.service.collaborative'
    ])
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

        // landing    
        $stateProvider.state('landing', {
            url: '/landing',
            templateUrl: 'templates/landing.html',
            controller: 'LandingController'
        });
        
        // loader
        $stateProvider.state('loader', {
            url: '/loader',
            templateUrl: 'templates/loader.html',
            controller: 'LoaderController'            
        });
        
        // Results
        $stateProvider.state('results', {
            url: '/results',
            templateUrl: 'templates/results.html',
            controller: 'ResultsController'            
        });
        
        // Error
        $stateProvider.state('error', {
            url: '/error',
            templateUrl: 'templates/error.html',
            controller: 'ErrorController'            
        });

        // routes
        $urlRouterProvider.otherwise('/landing');

    });

ngSubwayFreshTracks.run(function ($rootScope, $state, config, CMS) {

});

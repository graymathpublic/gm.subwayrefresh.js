'use strict';

angular.module('efsharp.ads.service.social', [])
        .factory('Social', [function () {

                // TODO: Inject CMS service and request data for social share meta

                var Social = {
                    facebookShare: function () {

                        return {
                            'shareImage': $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_thumbnail, // type = Url
                            'shareUrl': $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_url, // type = Url
                            'shareTitle': $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_title, // type = String
                            'shareCaption': $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_caption, // type = String
                            'shareDescription': $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_description, // type = String
                            //'metricsText': $rootScope.metrics.confirmFbShare, // type = String
                            'isDialog': false, // type = boolean,
                            'fbAppId': $rootScope.fb_app_id
                        };

                    },
                    twitterShare: function () {

                        return {
                            'shareTitle': $rootScope.cms.PayLoad.app_contents[2].contents[0].tweet_message, // type = string
                            // 'shareUrl': $rootScope.playlist.PayLoad.ShortUrl,
                            // 'metricsText': $rootScope.metrics.plTwShare
                        };

                    }

                };

                return Social;
            }])
/**
 * CMS Factory to get cms data
 */
angular.module("efsharp.ads.service.cms", ['ngSanitize', 'ngResource'])
    .factory('CMS', function($resource) {
        return $resource('//dev-api.efsharp-ads.com/cms/:key', {
            key: '@key'
        }, {
            get: {
                method: 'GET'
            }
        });
    });
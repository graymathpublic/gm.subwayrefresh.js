'use strict';

/**
 * Sweepstake
 * @param {type} param1
 * @param {type} param2
 */
ngSubwayFreshTracks.factory('Sweepstake',function($resource){
    return $resource('//dev-api.efsharp-ads.com/newsweepstakes', {},
        {
            save: { method: 'POST' }
        }
    );
});
/**
 * Collaborative API
 */
angular.module("efsharp.ads.service.collaborative", ['ngSanitize', 'ngResource'])
    .factory('CollaborativeTrack', function($resource) {
        /**
         * Collaborative Factory to search tracks and add tracks
         * @param {type} param1
         */
        return $resource('//dev-api.efsharp-ads.com/collaborative/track/:api/:query/:numberOfTracks/:region', {
            api: "@api",
            query: "@query",
            numberOfTracks: "@numberOfTracks",
            region: "@region"
        }, {
            get: {
                method: 'GET'
            }, // get with query search
            post: {
                method: 'POST'
            } // add track to collaborative playlist
        });
    }).factory('CollaborativePlaylist', function($resource) {
        /**
         * Collaborative Factory to get Collaborative playlist
         * @param {type} param1
         */
        return $resource('//dev-api.efsharp-ads.com/collaborative/:api/:numberOfTracks', {
            api: "@api",
            numberOfTracks: "@numberOfTracks"
        }, {
            get: {
                method: 'GET'
            }
        });
    }).factory('SpotifyTrack', ['$resource',
        function($resource) {

            return $resource('//dev-api.efsharp-ads.com/sptrack/:trackId', {
                trackId: '@trackId'
            }, {
                get: {
                    method: 'GET'
                }
            });
        }
    ])
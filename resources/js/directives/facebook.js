angular.module("efsharp.ads.directive.facebook", [])
        .directive('facebook', ['$rootScope', function ($rootScope) {
                return {
                    restrict: 'A',
                    scope: {
                        data: '=fbModel'
                    },
                    link: function ($scope, el, attrs) {
                        el.bind('click', function () {
                            if (typeof _f3m_do !== "undefined" && !angular.isUndefined(_f3m_do) && $scope.data.metricsText !== "") {
                                //_f3m_do($scope.data.metricsText, F3M_CAT_SOCIAL);
                            }
                            _f3m_do($rootScope.metrics.fbShare, F3M_CAT_SOCIAL);
                            var temp = "",
                                    url = $scope.data.shareUrl,
                                    image = $scope.data.shareImage,
                                    name = $scope.data.shareTitle,
                                    caption = $scope.data.shareCaption,
                                    description = $scope.data.shareDescription,
                                    redirect_url = 'https://www.facebook.com/';
                            if ($scope.data.isDialog) {
                                FB.ui({
                                    method: 'feed',
                                    name: name,
                                    caption: caption,
                                    description: description,
                                    link: url,
                                    picture: image
                                }, function (response) {
                                });
                            } else {
                                temp = "https://www.facebook.com/dialog/feed?app_id=" + $scope.data.fbAppId + "&display=popup&name=" + encodeURIComponent(name) + "&description=" + encodeURIComponent(description) + "&picture=" + encodeURIComponent(image) + "&link=" + encodeURIComponent(url) + "&redirect_uri=" + encodeURIComponent(redirect_url) + "&caption=" + encodeURIComponent(caption);
                                window.open(temp);
                                return true;
                            }
                        });
                    }
                };
            }]);
angular.module("efsharp.ads.directive.twitter", [])
        .directive('twitter', ['$rootScope', function ($rootScope) {
                return {
                    restrict: 'A',
                    scope: {
                        data: '=twitterModel'
                    },
                    link: function ($scope, el, attrs) {
                        el.bind('click', function () {
                            if (typeof _f3m_do !== "undefined" && !angular.isUndefined(_f3m_do) && $scope.data.metricsText !== "") {
                                //_f3m_do($scope.data.metricsText, F3M_CAT_SOCIAL);
                            }
                            _f3m_do($rootScope.metrics.twShare, F3M_CAT_SOCIAL);
                            window.open('https://twitter.com/intent/tweet?url=' + encodeURIComponent($scope.data.shareUrl) + '&text=' + encodeURIComponent($scope.data.shareTitle));
                        });
                    }
                };
            }]);
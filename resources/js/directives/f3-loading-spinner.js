angular.module('efsharp.ads.directive.loadingspinner', [])
    .directive('efsharpLoadingSpinner', [function () {
        return {
            restrict: 'A',
            scope: {
                loading: '='
            },
            template: '<svg class="f3-loading-spinner" width="100px" height="100px" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">' +
                        '<circle class="path" fill="none" stroke-width="10" stroke-linecap="square" cx="50" cy="50" r="30"></circle>' +
                        '</svg>',
            link: function (scope, el, iAttrs) {
                scope.$watch('loading', function (loading) {
                    if (loading) {
                        el.show();
                    } else {
                        el.hide();
                    }
                });
            }
        };
    }]);
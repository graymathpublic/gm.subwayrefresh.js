angular.module("efsharp.ads.directive.video", [])
        .directive('efsharpvideo', ['$rootScope', function ($rootScope) {
                return {
                    restrict: 'E',
                    scope: {
                        model: '='
                    },
                    replace: true,
                    template: '<div id="video-container"></div>',
                    link: function (scope, el, attrs) {


                        scope.$watch('model', function () {

                            /**
                             * JW Player integration
                             */
                            if (!angular.isUndefined(scope.model) && !angular.isUndefined($(el)) && !angular.isUndefined($(el).jwEfsharp)) {


                                var lVideoConfig = {
                                    isVidUrl: true,
                                    vidFile: [{file: scope.model.video_url}],
                                    vidImage: scope.model.video_image,
                                    vidFileName: scope.model.video_title,
                                    vidBOTR: scope.model.botr,
                                    vidAutostart: scope.model.auto_start,
                                    vidMute: scope.model.mute,
                                    vidMuteImage: 'https://s3.amazonaws.com/fsharp/videoplayer/clickForSound-static.png',
                                    vidRepeat: scope.model.repeat,
                                    vidWidth: scope.model.width,
                                    vidHeight: scope.model.height,
                                    onVideoDisplayClick: function () {
                                        if (scope.model.video_clickout !== "") {
                                            window.open(scope.model.video_clickout, "_blank");
                                        }
                                    },
                                    fallBackFlash: function () {
                                        // render image
                                        $(el).css({
                                            'background-image': 'url(' + scope.model.video_image + ')',
                                            'background-repeat': 'no-repeat',
                                            'background-size': '100% 100%',
                                            'height': scope.model.image_height
                                        });
                                    }
                                };

                                // start jwEfsharp wrapper
                                $(el).jwEfsharp(lVideoConfig);
                            }
                        });
                    }
                };
            }]);
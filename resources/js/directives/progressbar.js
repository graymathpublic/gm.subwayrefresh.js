angular.module("efsharp.ads.directive.progresscontrol", [])
        .directive('progressable', ['$rootScope', '$interval', function ($rootScope, $interval) {
                return {
                    restrict: 'A',
                    link: function (scope, el, attrs, controller) {

                        el.append('<span class="subway-progress-bar-text">' + scope.progressBarText + '</span>');
                        //has button look for button and remove disabled
                        if (angular.isUndefined(attrs.hasButton) || attrs.hasButton === "false") {
                            scope.hasButton = false;
                        } else {
                            scope.hasButton = true;
                        }
                        //set progress value to default if not set or set to 0
                        if (angular.isUndefined(attrs.value) || attrs.value === 0) {
                            scope.progressValue = 0;
                        } else {
                            scope.progressValue = attrs.value;
                        }
                        //set inline value if any inlineValue is true
                        if (angular.isUndefined(attrs.inlineValue) || attrs.inlineValue === "true") {
                            scope.inlineValue = true;
                        } else {
                            scope.inlineValue = false;
                        }
                        var lProgressInterval = $interval(function () {
                            var lEl = angular.element(".progress .progress-bar"),
                                    lMaxWidth = angular.element(".progress").width(),
                                    lCurWidth = parseInt(lEl.css('width'));
                            if (lCurWidth < lMaxWidth) {
                                lCurWidth = Math.round((lCurWidth / lMaxWidth) * 100);
                                var lNewWidth = lCurWidth + 3;
                                lCurWidth = lNewWidth;
                            } else {
                                if (scope.hasButton) {
                                    angular.element("button[progressButton]").animate({
                                        'width': '148px'
                                    }, 3000, function () {
                                        angular.element("button[progressButton]").removeAttr('disabled');
                                    });
                                }
                                $rootScope.$broadcast('onProgressComplete');
                                $interval.cancel(lProgressInterval);
                            }
                            if (scope.inlineValue) {
                                if (lCurWidth <= 100) {
                                    scope.progressValue = lCurWidth + "%";
                                } else {
                                    scope.progressValue = "100%";
                                }
                            }
                            lEl.css('width', lCurWidth + "%");
                        }, 200);

                    }
                };
            }]);
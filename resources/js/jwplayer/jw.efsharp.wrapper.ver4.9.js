
(function ($) {
    $.fn.jwEfsharp = function (options) {
        var settings = $.extend({
            isVidURL: false,
            sliceTime: 5,
            vidFile: '',
            vidImage: '',
            vidFileName: '',
            vidBOTR: '',
            vidAutostart: true,
            vidMute: false,
            vidMuteImage: 'http://s3.amazonaws.com/fsharp/videoplayer/clickForSound-static.png',
            vidRepeat: true,
            vidWidth: 640,
            vidHeight: 360,
            vidAspectRatio: '',
            fallBackFlash: null,
            onVideoDisplayClick: null,
            events_category: {
                AUTO_PLAY: 100,
                PAUSE: 101,
                PLAY: 102,
                REPLAY: 103,
                STOP: 104,
                COMPLETE: 105,
                TIMELOG: 106,
                IMPRESSION: 107,
                CLICKFORSOUND: 108,
                UNMUTE: 109,
                MUTE: 110
            }
        }, options);


        function loadSingleVideo() {

        }

        return this.each(function () {
            var selector = "#" + $(this).attr("id");
            var selector_title = $(this).attr("id")
            var counter = 0;
            var lastTime = 0;
            var isMute = settings.vidMute;
            var gotFiredOnce = false;
            var playTime = 0;
            var intervalRef = null;
            var intervalRefTemp1 = null;
            var intervalRefTemp2 = null;
            var intervalRefTemp3 = null;
            if (!settings.vidFileName || !settings.vidBOTR) {
                throw new Error('vidFileName is not defined: ' + settings.vidFileName, 'vidBOTR: ' + settings.vidBOTR);
                return;
            }

            if (!!document.createElement('video').canPlayType) {
                var player = null;
                if (settings.isVidURL) {
                    player = jwplayer(selector_title).setup({
                        file: settings.vidFile,
                        image: settings.vidImage,
                        autostart: settings.vidAutostart,
                        mute: settings.vidMute,
                        repeat: settings.vidRepeat,
                        width: settings.vidWidth,
                        height: settings.vidHeight,
                        aspectratio: settings.vidAspectRatio,
                        modes: [{type: 'html5'}]
                    });
                } else {
                    player = jwplayer(selector_title).setup({
                        levels: settings.vidFile,
                        image: settings.vidImage,
                        autostart: settings.vidAutostart,
                        mute: settings.vidMute,
                        repeat: settings.vidRepeat,
                        width: settings.vidWidth,
                        height: settings.vidHeight,
                        aspectratio: settings.vidAspectRatio,
                        modes: [{type: 'html5'}]
                    });
                }
                player.onReady(function () {
                    _f3m_do_vid("impression", settings.events_category.IMPRESSION);
                    if (settings.vidMute) {
                        if (settings.vidMuteImage !== "") {
                            $(selector).prepend('<div id="mute_' + selector_title + '" style="cursor:pointer; position: absolute;top: 0px;left: 0px;width: 100%;height:100%;background:url(' + settings.vidMuteImage + ') no-repeat center center;z-index: 999999;"></div>');
                        }
                    }
                    $(document).on("click", "#mute_" + selector_title, function () {
                        $(this).hide();
                        _f3m_do_vid("clickforsound", settings.events_category.CLICKFORSOUND);
                        intervalRefTemp1 = setInterval(function () {
                            if (MYCUSTOPLAYERREFERENCE != null && MYCUSTOPLAYERREFERENCE.getPlayerState() == 1) {
                                MYCUSTOPLAYERREFERENCE.unMute();
                                MYCUSTOPLAYERREFERENCE.setVolume(100);
                                clearInterval(intervalRefTemp1);
                            }
                        }, 500);
                        //callPlayer(selector_title+'_youtube','unMute');
                        player.setMute(false);
                    });
                });
                player.onDisplayClick(function (event) {
                    settings.onVideoDisplayClick();
                });

                player.onMute(function (event) {

                    if (isMute) {
                        isMute = false;
                        if (counter < 241) {
                            _f3m_do_vid("unmute", settings.events_category.UNMUTE);
                        }
                        intervalRefTemp2 = setInterval(function () {
                            if (MYCUSTOPLAYERREFERENCE != null) {
                                MYCUSTOPLAYERREFERENCE.unMute();
                                MYCUSTOPLAYERREFERENCE.setVolume(100);
                                clearInterval(intervalRefTemp2);
                            }
                        }, 500);
                        //callPlayer(selector_title+'_youtube','unMute');
                        //callPlayer(selector_title+'_youtube','setVolume',[100]);
                        player.setMute(false);
                    } else {
                        isMute = true;
                        if (counter < 241) {
                            _f3m_do_vid("mute", settings.events_category.MUTE);
                        }
                        intervalRefTemp3 = setInterval(function () {
                            if (MYCUSTOPLAYERREFERENCE != null) {
                                MYCUSTOPLAYERREFERENCE.mute();
                                clearInterval(intervalRefTemp3);
                            }
                        }, 500);
                        //callPlayer(selector_title+'_youtube','mute');	
                        player.setMute(true);
                    }
                });


                player.onPause(function (event) {
                    if (counter < 241) {
                        _f3m_do_vid("pause", settings.events_category.PAUSE);
                    }
                });

                player.onPlay(function (event) {
                    /*if(!gotFiredOnce){
                     gotFiredOnce = true;
                     callPlayer(selector_title+'_youtube','mute');												
                     }*/
                    if (!gotFiredOnce) {
                        gotFiredOnce = true;
                        intervalRef = setInterval(function () {
                            if (MYCUSTOPLAYERREFERENCE != null) {
                                MYCUSTOPLAYERREFERENCE.mute();
                                clearInterval(intervalRef);
                            }
                        }, 500);
                    }
                    var timeNow = new Date().getTime();

                    if (playTime != 0) {
                        if (timeNow - playTime > 2000) {
                            if (counter < 241) {
                                _f3m_do_vid("play", settings.events_category.PLAY);
                            }
                        }
                        playTime = timeNow;
                    } else {
                        playTime = timeNow;
                        if (counter < 241) {
                            _f3m_do_vid("play", settings.events_category.PLAY);
                        }
                    }
                });
                player.onComplete(function (event) {
                    if (counter < 241) {
                        _f3m_do_vid("complete", settings.events_category.COMPLETE);
                    }
                });
                player.onTime(function (event) {
                    var timeFloor = Math.floor(event.position);

                    if (timeFloor !== 0 && timeFloor % 5 == 0) {
                        var time = parseFloat(event.position);
                        var timeFloor = Math.floor(event.position);

                        if (timeFloor !== lastTime && time > lastTime) {
                            lastTime = timeFloor;
                            counter += 5;
                            if (counter < 241) {
                                _f3m_do_vid("" + counter, settings.events_category.TIMELOG);
                            }
                        }
                    } else {
                        if (timeFloor == 0 && lastTime != 0)
                            lastTime = 0;
                    }
                });
            } else {
                if (settings.fallBackFlash !== null) {
                    settings.fallBackFlash();
                }
            }
        });

    };
}(jQuery));
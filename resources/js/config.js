'use strict';

angular.module('ngSubwayFreshTracks.config', [])
    .constant('config', {

        /**
         * CMS credentials
         */
        cms: {
            'en': {
                'api_key': '2f751735e15da220f25bec101c111f5c'
            },
            'fr': {
                'api_key': '5564b38a3ec5caef47cd4be5cdb339bf'
            }
        },

        /**
         * Collaborative API Credentials
         */
        collaborative: {
            'api_key': 'a7a6bdce2147f52e9c7447e3d9066a8a'
        },

        /**
         * Submission data schema
         */
        submissionData: {
            campaign: 'f3.ng.subway.freshtracks',
            formData: [],
            formValidator: [
                {
                    "first_name": "required",
                    "last_name": "required",
                    "email": "required",
                    "accepted_terms": "required",
                    "opt_in": "not required"
                }
            ]
        }

    });
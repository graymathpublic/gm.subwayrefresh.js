ngSubwayFreshTracks.controller('LandingController', [
    '$scope', '$rootScope', '$state', '$q', 'CollaborativeTrack', 'CollaborativePlaylist', 'config', 'SpotifyTrack',
    function ($scope, $rootScope, $state, $q, CollaborativeTrack, CollaborativePlaylist, config, SpotifyTrack) {
        $scope.search = {
            query: ''
        };
        $scope.loadCount = 20;
        $rootScope.selectedTrack = {};
        $rootScope.slides = {
            0: [],
            1: [],
            2: [],
            3: []
        };
        $scope.trackResults = [];
        $scope.hasTrackResults = false;

        /**
         * Select track from search results
         * @param  {object} track
         */
        $scope.selectTrack = function (track) {
            // set selected track
            $rootScope.selectedTrack = track;

            // create a collaborative track and save it to playlist
            var selectedTrack = new CollaborativeTrack({
                api: config.collaborative.api_key,
                trackuri: track.Id,
            });
            selectedTrack.$save();

            // go to loader state
            $state.go('loader');
        };

        /**
         * Reset track search results
         */
        $scope.resetSearch = function () {
            $scope.trackResults = [];
            $scope.loadCount = 20;
            $scope.hasTrackResults = false;
            $scope.noResults = false;
        };

        /**
         * Populate track results in list
         * @param  {object} response
         */
        $scope.populateTrackResults = function (res) {

            var tracks = res.PayLoad;
            // stop loader
            $scope.isLoading = false;
            $scope.noResults = false;
            // clear current results
            $scope.trackResults = [];
            // populate [loadCount] tracks
            for (var i = 0; i < $scope.loadCount; i++) {
                if (!angular.isUndefined(tracks[i])) {
                    $scope.trackResults.push(tracks[i]);
                }
            }

            // stop populating if we have 40 or more results
            if ($scope.trackResults.length >= 40) {
                $scope.hasTrackResults = true;
            } else if ($scope.trackResults.length < 1) {
                // set no results
                $scope.noResults = true;
            }
        };

        /**
         * Load more results
         * @param  {number} addAmount
         */
        $scope.loadMoreResults = function (addAmount) {
            // increase the load count
            $scope.loadCount += addAmount;
            // repeat the current query
            $scope.loadResults($scope.search.query);
            // scroll to new selections
            var height = $('.sw-dropdown-menu li').first().outerHeight();
            var offset = height * $scope.loadCount;
            $('.sw-dropdown-menu').scrollTop(offset);
        };


        /**
         * Load results
         * @param  {object} query
         */
        $scope.loadResults = function (query) {
            // start loader
            $scope.isLoading = true;

            // get tracks
            CollaborativeTrack.get({
                api: config.collaborative.api_key,
                query: query,
                numberOfTracks: $scope.loadCount,
                region: 'US'
            }, $scope.populateTrackResults);
        };

        /**
         * Search song API for tracks from query
         * @param  {[type]} $event [description]
         * @return {[type]}        [description]
         */
        $scope.onSearchChange = function (query, $event) {
            $scope.resetSearch();
            $scope.loadResults(query);
        };

        /**
         * Populate recently added tracks
         * @param  {object} data
         */
        $scope.populateRecentlyAdded = function (tracks) {
            for (var i = 0; i < 5; i++) {
                $scope.slides[0].push(tracks[i]);
                $scope.slidesLoaded = true;
            }
        };

        $scope.getPlaylist(5, $scope.populateRecentlyAdded);

        $scope.$on('$viewContentLoaded', function (event) {
            _f3m_do($rootScope.metrics.impLanding, F3M_CAT_INTERACTIONS);
        });

        $scope.onSearchArtist = function () {
            _f3m_do($rootScope.metrics.artistSearch, F3M_CAT_INTERACTIONS);
        };

        $rootScope.onVisit = function () {
            _f3m_do($rootScope.metrics.footerVisit, F3M_CAT_INTERACTIONS);
        };
        $rootScope.onLike = function () {
            _f3m_do($rootScope.metrics.footerLike, F3M_CAT_INTERACTIONS);
        };
        $rootScope.onFollow = function () {
            _f3m_do($rootScope.metrics.footerFollow, F3M_CAT_INTERACTIONS);
        };

    }]);
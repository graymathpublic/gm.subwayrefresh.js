
ngSubwayFreshTracks.controller('ErrorController', function ($scope, $rootScope) {

    $rootScope.cms.$promise.then(function (res) {
        if (angular.isDefined(res.PayLoad.app_contents[4].contents[0])) {
            $scope.errorText = res.PayLoad.app_contents[4].contents[0];
        }
    });

    $scope.$on('$viewContentLoaded', function (event) {
        _f3m_do($rootScope.metrics.impError, F3M_CAT_INTERACTIONS);
    });

    $scope.onTryAgain = function ($event) {
        _f3m_do($rootScope.metrics.buttonTryAgain, F3M_CAT_INTERACTIONS);
    };

});
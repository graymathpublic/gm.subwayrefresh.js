ngSubwayFreshTracks.controller('LoaderController', function ($scope, $rootScope, $state) {

    $scope.loadingText = $rootScope.cms.PayLoad.app_contents[0].contents[0].subheadline;
    $scope.progressBarText = $rootScope.cms.PayLoad.app_contents[0].contents[0].loader_text;
    $scope.continueButton = $rootScope.cms.PayLoad.app_contents[0].contents[0].continue_button_label;

    $scope.videoDidPlay = false;
    $rootScope.$on('onProgressComplete', function () {
        $scope.videoDidPlay = true;
    });

    if (!angular.isDefined($rootScope.selectedTrack)) {
        $state.transitionTo('landing');
    }

    $scope.onNextClick = function () {
        _f3m_do($rootScope.metrics.buttonContinue, F3M_CAT_INTERACTIONS);
        $state.transitionTo('results');
    };

    $scope.$on('$viewContentLoaded', function (event) {
        _f3m_do($rootScope.metrics.impLoader, F3M_CAT_INTERACTIONS);
    });

});

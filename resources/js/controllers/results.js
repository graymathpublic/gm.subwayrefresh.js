
ngSubwayFreshTracks.controller('ResultsController', function ($scope, $rootScope, $state, $modal, Social, $sce) {

    $scope.fbConfirmationData = Social.facebookShare;
    $scope.twitterData = Social.twitterShare;
    $rootScope.slides = {
        0: [],
        1: [],
        2: [],
        3: []
    };
    $scope.slidesLoaded = false;

    $rootScope.$watch('cms', function () {
        $rootScope.cms.$promise.then(function () {
            $scope.subscriptionText = $rootScope.cms.PayLoad.app_contents[2].contents[0].subscription_text;
            $scope.enterButton = $rootScope.cms.PayLoad.app_contents[2].contents[0].enter_button_label;
            $scope.trackText = $rootScope.cms.PayLoad.app_contents[2].contents[0].track_label;
            $scope.addSongText = $rootScope.cms.PayLoad.app_contents[2].contents[0].add_another_song_text;
            $scope.listenButton = $rootScope.cms.PayLoad.app_contents[2].contents[0].listen_button_label;
            $scope.headline = $rootScope.cms.PayLoad.app_contents[3].contents[0].headline;
            $scope.subheadline = $rootScope.cms.PayLoad.app_contents[3].contents[0].subheadline;
            $scope.firstNameLabel = $rootScope.cms.PayLoad.app_contents[3].contents[0].first_name_label;
            $scope.lastNameLabel = $rootScope.cms.PayLoad.app_contents[3].contents[0].last_name_label;
            $scope.emailLabel = $rootScope.cms.PayLoad.app_contents[3].contents[0].email_label;
            $scope.termsText = $rootScope.cms.PayLoad.app_contents[3].contents[0].terms_text;
            $scope.termsText2 = $rootScope.cms.PayLoad.app_contents[3].contents[0].terms_text_2;
            $scope.signUpText = $rootScope.cms.PayLoad.app_contents[3].contents[0].signup_text;
            $scope.submitButton = $rootScope.cms.PayLoad.app_contents[3].contents[0].submit_button_label;
            $scope.termsHeadline = $rootScope.cms.PayLoad.app_contents[3].contents[0].terms_and_conditions_headline;
            $scope.tcText = $rootScope.cms.PayLoad.app_contents[3].contents[0].terms_and_conditions_text;
            $scope.backLabel = $rootScope.cms.PayLoad.app_contents[3].contents[0].back_label;
            $scope.cancelLabel = $rootScope.cms.PayLoad.app_contents[3].contents[0].cancel_label;
            $rootScope.tweet = $rootScope.cms.PayLoad.app_contents[2].contents[0].tweet_message;
            $rootScope.fb_url = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_url;
            $rootScope.fb_title = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_title;
            $rootScope.fb_caption = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_caption;
            $rootScope.fb_description = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_description;
            $rootScope.fb_image = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_thumbnail;
        });
    });

    /**
     * Convert to play.spotify.com
     * @param {type} sp_uri
     * @returns {lValue}
     */
    $scope.toPlaySpotify = function (sp_uri) {
        var lValue = ('https://play.' + sp_uri.replace(/:/ig, '/')).replace('.spotify/', '.spotify.com/');
        return lValue;
    };

    /**
     * Open Spotify track in app or mobile browser
     */
    $scope.listenNow = function () {
        // $scope.logMetrics($rootScope.metrics.listen, F3M_CAT_INTERACTIONS);
        if ($(window).width() < 540) {
            _f3m_do($rootScope.metrics.buttonListen, F3M_CAT_CLICKOUTS);
            window.open($scope.toPlaySpotify($rootScope.playlistId), '_blank');
        } else {
            _f3m_do($rootScope.metrics.buttonListen, F3M_CAT_CLICKOUTS, function () {
                document.getElementById('spotify').setAttribute('src', $rootScope.playlistId)
                //window.open($rootScope.playlistId, '_blank');
            });
        }
    };


    /* add another song action*/
    $scope.selectAnotherSong = function () {
        _f3m_do($rootScope.metrics.addAnother, F3M_CAT_INTERACTIONS);
        $rootScope.playlist = null;
        $rootScope.track_obj = null;
        $state.transitionTo('landing');
    };

    // if (angular.isUndefined($rootScope.selectedTrack)) {
    //     $state.go('landing');
    // }

    $scope.openSignupModal = function () {
        _f3m_do($rootScope.metrics.buttonEnter, F3M_CAT_INTERACTIONS);
        _f3m_do($rootScope.metrics.impEntered, F3M_CAT_INTERACTIONS);
        var modalInstance = $modal.open({
            templateUrl: 'templates/signup-modal.html',
            controller: 'SignupController',
            size: 'sm'
        });
    };

    $scope.populateSlides = function (tracks) {
        var slide = -1;
        for (var i = 0; i < 20; i++) {
            if (i % 5 == 0) {
                slide++;
            }
            $scope.slides[slide].push(tracks[i]);
        }
        $scope.slidesLoaded = true;
    };

    $scope.getPlaylist(20, $scope.populateSlides);

    $rootScope.fb_playlist = function () {
        _f3m_do($rootScope.metrics.fbShare, F3M_CAT_SOCIAL);
        $rootScope.fbShare($rootScope.fb_url, $rootScope.fb_title, $rootScope.fb_image, $rootScope.fb_caption, $rootScope.fb_desc);
    };
    $rootScope.tw_playlist = function () {
        _f3m_do($rootScope.metrics.twShare, F3M_CAT_SOCIAL);
        $rootScope.twShare($rootScope.tweet_message);
    };

    $scope.$on('$viewContentLoaded', function (event) {
        _f3m_do($rootScope.metrics.imgResult, F3M_CAT_INTERACTIONS);
    });
});

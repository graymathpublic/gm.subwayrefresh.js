
ngSubwayFreshTracks.controller('AppController', [
    '$scope', '$rootScope', '$q', 'CMS', 'CollaborativePlaylist', 'SpotifyTrack', 'config',
    function ($scope, $rootScope, $q, CMS, CollaborativePlaylist, SpotifyTrack, config) {

        $rootScope.fb_url = "";
        $rootScope.fb_title = "";
        $rootScope.fb_caption = "";
        $rootScope.fb_desc = "";
        $rootScope.fb_image = "";

        $rootScope.tweet_message = "";

        $rootScope.setCMS = function (api_key) {
            $rootScope.cms = CMS.get({key: api_key}, function (res) {
                $rootScope.cmsIsLoaded = true;
                // $scope.loadMetricsPreReq();
                // $scope.onHeaderFbShare();
                console.log(res);

                $rootScope.fb_url = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_url;
                $rootScope.fb_title = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_title;
                $rootScope.fb_caption = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_caption;
                $rootScope.fb_desc = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_description;
                $rootScope.fb_image = $rootScope.cms.PayLoad.app_contents[2].contents[0].fb_image;

                $rootScope.tweet_message = $rootScope.cms.PayLoad.app_contents[2].contents[0].tweet_message;
            }, function (error) {
                $state.transitionTo('error');
            });
        };

        $rootScope.$on('LANG_CHANGE', function (e, data) {
            $rootScope.setCMS(config.cms[data.lang.code].api_key);
        });

        $rootScope.setCMS(config.cms['en'].api_key);
        $rootScope.status = {
            isopen: false
        };
        $rootScope.language = {
            currentLang: 0,
            lang: [
                {
                    text: 'English',
                    code: 'en'
                },
                {
                    text: 'Français',
                    code: 'fr'
                }
            ]
        };
        $rootScope.fb_app_id = '724431650979676';
        $rootScope.metrics_app_id = 'f3_ng_subway_freshtracks';
        $rootScope.metrics = {
            'impLanding': '1.00 Landing Page impression',
            'langToggle': '1.01 Language Toggle',
            'artistSearch': '1.02 Artist Search',
            'footerVisit': '1.03 Visit Subway.com footer link',
            'footerLike': '1.04 Like Us on Facebook footer link',
            'footerFollow': '1.05 Follow Us on Twitter footer link',
            'impLoader': '2.00 Loader Page Impression',
            'buttonContinue': '2.01 Continue',
            'imgResult': '3.00 Results Page Impression',
            'buttonEnter': '3.01 Enter to Win',
            'buttonListen': '3.02 Listen Now',
            'fbShare': '3.03 Facebook playlist share',
            'twShare': '3.04 Twitter playlist share',
            'addAnother': '3.05 Add Another Song',
            'playlistNav': '3.06 Playlist navigation tools',
            'impEntered': '4.00 Enter to Win box impression',
            'fName': '4.01 First Name',
            'lName': '4.02 Last Name',
            'email': '4.03 Email Address',
            "tncLink": '4.04 Terms and Conditions link',
            'tncCheckbox': '4.05 Terms and Conditions check box',
            'offersCheckbox': '4.06 News/Updates check box',
            'buttonSubmit': '4.07 Submit button',
            'impSuccess': '5.00 Successful entry impression',
            'backToPl': '5.01 Back to the Playlist link',
            'impError': '6.00 Error Page impression',
            'buttonTryAgain': '6.01 Try Again button'
        };
        $rootScope.slidesLoaded = false;
        $rootScope.slides = {
            0: [],
            1: [],
            2: [],
            3: []
        };
        $rootScope.recentlyAdded = [];

        $rootScope.$watch('cms', function () {
            $rootScope.cms.$promise.then(function (res) {

                if (angular.isDefined(res.PayLoad.app_contents[5].contents[0])) {
                    $rootScope.footer = res.PayLoad.app_contents[5].contents[0];
                }

                if (angular.isDefined(res.PayLoad.app_contents[1])) {
                    $rootScope.landing = res.PayLoad.app_contents[1].contents[0];
                }

                $rootScope.playlistId = res.PayLoad.app_contents[2].contents[0].playlist_id;

                $rootScope.videoModel = {
                    video_url: res.PayLoad.app_contents[0].contents[0].video_url,
                    video_image: res.PayLoad.app_contents[0].contents[0].video_thumbnail,
                    video_title: res.PayLoad.app_contents[0].contents[0].video_filename,
                    botr: res.PayLoad.app_contents[0].contents[0].video_botr,
                    video_clickout: '',
                    auto_start: true,
                    mute: res.PayLoad.app_contents[0].contents[0].video_mute,
                    repeat: false,
                    height: '100%',
                    width: '100%',
                    image_height: '180px'
                };
            });

        });
        /**
         * Filter for showing unselected languages
         * @param  {object} element
         */
        $scope.unselectedLangs = function (element) {
            return $rootScope.language.lang[$rootScope.language.currentLang].code !== element.code;
        };

        /**
         * Select a language
         * @param  {object} lang
         */
        $scope.selectLang = function (lang) {
            _f3m_do($rootScope.metrics.langToggle, F3M_CAT_INTERACTIONS);
            $rootScope.language.lang.filter(function (l, i) {
                if (lang.code == l.code) {
                    $rootScope.language.currentLang = i;
                    $scope.status.isopen = !$scope.status.isopen;
                    $rootScope.$broadcast('LANG_CHANGE', {
                        lang: lang
                    });
                }
            });
        };

        /**
         * Get the album art for a collection of tracks
         * @param  {array} tracks
         * @return {array} tracksWithAlbumArt
         * @deps SpotifyTrack service
         */
        $scope.getAlbumArt = function (tracks) {
            var dfd = $q.defer();
            var tracksWithAlbumArt = [];

            if (tracks.length) {
                angular.forEach(tracks, function (v, k) {
                    var id = v.SpTrackId.split(':').pop();
                    SpotifyTrack.get({
                        trackId: id
                    }).$promise.then(function (res) {
                        if (res.tracks[0].album.images[0]) {
                            angular.extend(v, {
                                image: res.tracks[0].album.images[0]
                            });
                        }
                    });
                    tracksWithAlbumArt.push(v);
                });
                dfd.resolve(tracksWithAlbumArt);
            }
            return dfd.promise;
        };

        /**
         * Get the collaborative playlist, with album art
         * @param  {number} numberOfTracks
         * @param  {function} success
         * @deps   CollaborativePlaylist service
         */
        $scope.getPlaylist = function (numberOfTracks, success) {
            CollaborativePlaylist.get({
                api: config.collaborative.api_key,
                numberOfTracks: numberOfTracks
            }, function (res) {
                if (res.PayLoad.Tracks.length > 0) {
                    $scope.getAlbumArt(res.PayLoad.Tracks)
                            .then(function (tracksWithAlbumArt) {
                                success(tracksWithAlbumArt);
                            });
                }
            }, function () {
                $state.go('error');
            });
        };

        $rootScope.fbShare = function (url, name, image, caption, text) {
            window.open("https://www.facebook.com/dialog/feed?app_id=" + $rootScope.fb_app_id + "&display=popup&name=" + encodeURIComponent(name) + "&description=" + encodeURIComponent(text) + "&picture=" + encodeURIComponent(image) + "&caption=" + encodeURIComponent(caption) + "&link=" + encodeURIComponent(url) + "&redirect_uri=" + encodeURIComponent("http://www.facebook.com/"));
            return true;
        };

        $rootScope.twShare = function (message) {
            var width = 575,
                    height = 400,
                    left = ($(window).width() - width) / 2,
                    top = ($(window).height() - height) / 2,
                    opts = 'status=1' +
                    ',width=' + width +
                    ',height=' + height +
                    ',top=' + top +
                    ',left=' + left;
            window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(message), 'twitter', opts);
        };

    }]);

angular.module('ng').filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value)
            return '';

        max = parseInt(max, 10);
        if (!max)
            return value;
        if (value.length <= max)
            return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});

$(document).on("click", "a.carousel-control.right", function () {
    _f3m_do('3.06 Playlist navigation tools', F3M_CAT_INTERACTIONS);
});
$(document).on("click", "a.carousel-control.left", function () {
    _f3m_do('3.06 Playlist navigation tools', F3M_CAT_INTERACTIONS);
});

onFocus = function (element) {
    if (element.value == '') {
        element.setAttribute('placeholder', '');
    }
};
onBlur = function (element) {
    var placeholder = element.getAttribute('data-place');
    if (element.value == '') {
        element.setAttribute('placeholder', placeholder);
    }
};

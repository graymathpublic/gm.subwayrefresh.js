'use strict';

ngSubwayFreshTracks.controller('SignupController', ['$scope', '$rootScope', '$modalInstance', '$state', 'Sweepstake', 'config', function ($scope, $rootScope, $modalInstance, $state, Sweepstake, config) {

        $scope.formSubmitted = false;
        $scope.submission = {};
        $scope.onTermsPage = false;
        $scope.successPage = false;

        $scope.submitForm = function (valid, submission) {
            $scope.formSubmitted = true;

            var sweepstakes = new Sweepstake();
            if (valid && angular.isDefined(submission)) {

                // configure data object
                config.submissionData.formData.push(submission);
                angular.extend(sweepstakes, config.submissionData);

                sweepstakes.$save()
                        .then(function () {
                            _f3m_do($rootScope.metrics.buttonSubmit, F3M_CAT_INTERACTIONS);
                            _f3m_do($rootScope.metrics.impSuccess, F3M_CAT_INTERACTIONS);
                            $scope.successPage = true;
                        }, function () {
                            $modalInstance.close('cancel');
                            $state.go('error');
                        });
            }
        };

        $scope.cancel = function () {
            _f3m_do($rootScope.metrics.backToPl, F3M_CAT_INTERACTIONS);
            $modalInstance.close('cancel');
        };

        // Terms Page Navigation
        // ---------------------------------------------
        $scope.enterTermsPage = function () {
            _f3m_do($rootScope.metrics.tncLink, F3M_CAT_INTERACTIONS);
            $scope.onTermsPage = true;
        };

        $scope.onTerms = function ($event) {
            _f3m_do($rootScope.metrics.tncCheckbox, F3M_CAT_INTERACTIONS);
        };
        $scope.onOffers = function ($event) {
            _f3m_do($rootScope.metrics.offersCheckbox, F3M_CAT_INTERACTIONS);
        };

        $scope.exitTermsPage = function () {
            $scope.onTermsPage = false;
        };

        $scope.onfName = function () {
            _f3m_do($rootScope.metrics.fName, F3M_CAT_INTERACTIONS);
        };

        $scope.onlName = function () {
            _f3m_do($rootScope.metrics.lName, F3M_CAT_INTERACTIONS);
        };

        $scope.onEmail = function () {
            _f3m_do($rootScope.metrics.email, F3M_CAT_INTERACTIONS);
        };

    }]);